package com.epam.userservice.controller;

import com.epam.common.models.User;
import com.epam.userservice.exception.InvalidFieldException;
import com.epam.userservice.exception.UserServiceException;
import com.epam.userservice.exception.UserServiceResponse;
import com.epam.userservice.exception.UserServiceResponseMessage;
import com.epam.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/{user_id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable("user_id") String userId) throws UserServiceException {
        return userService.getUser(userId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> addUser(@RequestBody @Validated User user, BindingResult bindingResult) throws InvalidFieldException {
        validRequest(bindingResult);
        userService.addUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{user_id}", method = RequestMethod.PUT)
    public User updateBook(@PathVariable("user_id") @NotEmpty String userId,
                           @RequestBody User user) throws UserServiceException {
        return userService.updateUser(userId, user);
    }

    @RequestMapping(path = "/{user_id}", method = RequestMethod.DELETE)
    public ResponseEntity<UserServiceResponse> deleteBook(@PathVariable("user_id") String userId) throws UserServiceException {
        userService.deleteUser(userId);
        return new ResponseEntity<>(new UserServiceResponse(HttpStatus.OK.getReasonPhrase(), String.format(
                UserServiceResponseMessage.DELETE_SUCCESSFUL.getMessage(), userId)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<User> getUsers() {
        return userService.getUsers();
    }

    private void validRequest(BindingResult bindingResult) throws InvalidFieldException {
        if (bindingResult.hasErrors()) {
            throw new InvalidFieldException(bindingResult.getFieldErrors());
        }
    }
}
