package com.epam.userservice.service;

import com.epam.common.models.User;
import com.epam.userservice.exception.UserServiceException;
import com.epam.userservice.exception.UserServiceResponseMessage;
import com.epam.userservice.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<User> getUser(String userId) throws UserServiceException {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserServiceException(
                HttpStatus.BAD_REQUEST.getReasonPhrase(), UserServiceResponseMessage.USER_NOT_FOUND.getMessage()));
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public void addUser(User user) {
        user.setUserId(UUID.randomUUID().toString());
        userRepository.save(user);
    }

    public void deleteUser(String userId) {
        try {
            userRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            throw new UserServiceException(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                    String.format(UserServiceResponseMessage.UNABLE_TO_PERFORM_DELETE.getMessage(), userId));
        }
    }

    public User updateUser(String userId, User newUser) {
        User oldUser = userRepository.findById(userId).orElseThrow(() -> new UserServiceException(
                HttpStatus.BAD_REQUEST.getReasonPhrase(), UserServiceResponseMessage.UNABLE_TO_UPDATE_USER.getMessage()));
        if (StringUtils.isNotBlank(newUser.getLastName())) oldUser.setLastName(newUser.getLastName());
        oldUser.setEmail(newUser.getLastName());
        return userRepository.save(oldUser);
    }
}
