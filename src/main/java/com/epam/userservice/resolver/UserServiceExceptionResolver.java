package com.epam.userservice.resolver;

import com.epam.userservice.exception.InvalidFieldException;
import com.epam.userservice.exception.RequestFieldError;
import com.epam.userservice.exception.UserServiceException;
import com.epam.userservice.exception.UserServiceResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class UserServiceExceptionResolver {

    @ExceptionHandler(InvalidFieldException.class)
    public ResponseEntity<List<RequestFieldError>> resolveFieldError(InvalidFieldException invalidFieldException) {
        Map<String, RequestFieldError> errors = new HashMap<>();
        invalidFieldException.getFieldErrors().forEach(fieldError -> {
            if (errors.get(fieldError.getField()) == null) {
                errors.put(fieldError.getField(), new RequestFieldError(fieldError.getField(),
                        fieldError.getDefaultMessage()));
            }
        });
        return new ResponseEntity<>(new ArrayList<>(errors.values()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserServiceException.class)
    public ResponseEntity<UserServiceResponse> resolveServiceException(UserServiceException userServiceException) {
        return new ResponseEntity<>(new UserServiceResponse(userServiceException.getReasonCode(),
                userServiceException.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<List<RequestFieldError>> resolveInputParametersErrors(ConstraintViolationException cvException) {
        List<RequestFieldError> errors = new ArrayList<>();
        cvException.getConstraintViolations().forEach(constraintViolation -> errors.add(
                new RequestFieldError(constraintViolation.getMessage(), "INVALID RESOURCE ID")));
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
}
