package com.epam.userservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@AllArgsConstructor
public class UserServiceException extends RuntimeException {

    @Getter
    private String reasonCode;
    private String message;

    @Override
    public String getMessage() {
        return this.message;
    }
}
