package com.epam.userservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserServiceResponseMessage {

    USER_NOT_FOUND("No user found against provided user id"),
    UNABLE_TO_UPDATE_USER("Update failed: No book found against provided user id"),
    UNABLE_TO_ADD_ITEM("Unable to add book"),
    UNABLE_TO_PERFORM_DELETE("Delete unsuccessful: No user found against user id %s"),
    DELETE_SUCCESSFUL("Delete successful: User against user id %s has been deleted");

    private String message;
}
