package com.epam.userservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.FieldError;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class InvalidFieldException extends RuntimeException {

    private List<FieldError> fieldErrors;
}
