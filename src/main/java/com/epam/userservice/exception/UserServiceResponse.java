package com.epam.userservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class UserServiceResponse {

    private String reasonPhrase;
    private String format;

}
